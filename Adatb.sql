create table Jog(
    jognev VARCHAR(20),
    PRIMARY KEY(jognev)
);

create table Kategoria(
    nev VARCHAR(20),
    felkapott NUMBER(3),
    PRIMARY KEY(nev)
);

create table Termek(
    termekid NUMBER,
    nev VARCHAR(20),
    ar NUMBER(6),
    hazhozar NUMBER(4),
    keszlet NUMBER(6),
    katnev VARCHAR(20),
    PRIMARY KEY(termekid),
    FOREIGN KEY(katnev) REFERENCES KATEGORIA(NEV),
    CHECK (ar > 0)
);

create table Felhasznalo(
    azonosito VARCHAR(10) NOT NULL,
    pwd VARCHAR(20) NOT NULL,
    email VARCHAR(60),
    utonev VARCHAR(100),
    elonev VARCHAR(100),
    jognev VARCHAR(20),
    varos VARCHAR(60),
	egyenleg NUMBER(10),
    PRIMARY KEY(azonosito, email),
    FOREIGN KEY(jognev) REFERENCES JOG(JOGNEV)
);

create table Rendeles(
    rendid NUMBER,
    termekid NUMBER,
    mennyit NUMBER(3),
	nev VARCHAR(10),
	kifizetve NUMBER(2),
    PRIMARY KEY(rendid),
    FOREIGN KEY(termekid) REFERENCES TERMEK(TERMEKID)
);

create table Szamla(
    szamlaszam NUMBER(6) NOT NULL,
    vegosszeg NUMBER,
    userid VARCHAR(10),
    useremail VARCHAR(60),
	kiszallitva NUMBER(2),
	hatarido DATE DEFAULT SYSDATE,
    PRIMARY KEY(szamlaszam),
    FOREIGN KEY(userid, useremail) REFERENCES FELHASZNALO(AZONOSITO, EMAIL)
);

INSERT into JOG (JOGNEV) values('Admin');
INSERT into JOG (JOGNEV) values('User');

INSERT into KATEGORIA (NEV) values('Photo');
INSERT into KATEGORIA (NEV) values('Tv');
INSERT into KATEGORIA (NEV) values('Computer');
INSERT into KATEGORIA (NEV) values('Garden');
INSERT into KATEGORIA (NEV) values('Home');
INSERT into KATEGORIA (NEV) values('Mobil');
INSERT into KATEGORIA (NEV) values('Dress');
INSERT into KATEGORIA (NEV) values('Other');

INSERT into TERMEK values(0,'Kamera1',10000,500,5,'Photo');
INSERT into TERMEK values(1,'Nikon',52000,600,3,'Photo');
INSERT into TERMEK values(2,'Nikon2',60000,800,6,'Photo');
INSERT into TERMEK values(3,'SonyCam',30000,420,4,'Photo');
INSERT into TERMEK values(4,'SamsungTv',100000,1000,8,'Tv');
INSERT into TERMEK values(5,'LGTv',91000,900,6,'Tv');
INSERT into TERMEK values(6,'HorizonTv',60000,800,5,'Tv');
INSERT into TERMEK values(7,'PanasonicTv',95000,999,7,'Tv');
INSERT into TERMEK values(8,'Monitor',24000,320,3,'Computer');
INSERT into TERMEK values(9,'Hangszoro',9500,500,6,'Computer');
INSERT into TERMEK values(10,'Billentyuzet',4000,230,34,'Computer');
INSERT into TERMEK values(11,'Eger',6700,321,52,'Computer');
INSERT into TERMEK values(12,'Tuja',300,400,33,'Garden');
INSERT into TERMEK values(13,'Gyumolcsfa',3000,300,123,'Garden');
INSERT into TERMEK values(14,'Virag',200,200,110,'Garden');
INSERT into TERMEK values(15,'Eszkoz',7800,320,12,'Garden');
INSERT into TERMEK values(16,'Porszivo',30000,1200,12,'Home');
INSERT into TERMEK values(17,'Szonyeg',20000,4000,32,'Home');
INSERT into TERMEK values(18,'Szekreny',50000,4999,12,'Home');
INSERT into TERMEK values(19,'Mosogep',70000,3400,2,'Home');
INSERT into TERMEK values(20,'SamsungMobil',120000,2300,40,'Mobil');
INSERT into TERMEK values(21,'LGMobil',87000,2213,21,'Mobil');
INSERT into TERMEK values(22,'AppleMobil',300000,9000,40,'Mobil');
INSERT into TERMEK values(23,'HonorMobil',110000,2333,12,'Mobil');
INSERT into TERMEK values(24,'GucciPolo',70000,2400,1,'Dress');
INSERT into TERMEK values(25,'KorsNadrag',43000,3400,2,'Dress');
INSERT into TERMEK values(26,'JordanCipo',32000,3200,12,'Dress');
INSERT into TERMEK values(27,'KamuAdidasMelegito',200,10,1,'Dress');
INSERT into TERMEK values(28,'Csavar',1200,110,60,'Other');
INSERT into TERMEK values(29,'Taska',8000,800,10,'Other');
INSERT into TERMEK values(30,'DartsTabla',12000,210,4,'Other');
INSERT into TERMEK values(31,'Borond',5000,500,6,'Other');

INSERT into FELHASZNALO values('Viki','asd','viki@ancsin.edit','Viki','Ancsin','Admin','Bekescsaba',0);
INSERT into FELHASZNALO values('Marci','123','marci@varga.nincs','Marci','Varga','Admin','Szeged',0);
INSERT into FELHASZNALO values('Anett','dsa','anett@mari.timea','Anett','Mari','User','Bekescsaba',0);
INSERT into FELHASZNALO values('Zsombor','321','zsombor@peto.nemtom','Zsombor','Peto','User','Ujszeged',0);
INSERT into FELHASZNALO values('Pista','lfksjdfksdf','pista@pistike.hu','Pista','Vok','User','Bugyi',0);
INSERT into FELHASZNALO values('Jancsi','kldfdjafasdkfsjadf','jancsi@mezeskalacs.haz','Jancsi','Anevem','User','Erdo',0);
INSERT into FELHASZNALO values('Juliska','ksadfjlkdsjfdsf','juliska@nemtom.mi','Juliska','Anevem','User','Erdo',0);
INSERT into FELHASZNALO values('Csabi','klfaskdfd','csaba@major.nincs','Csaba','Major','User','Szeged',0);
INSERT into FELHASZNALO values('Feci','3k4j3k4j2l34j2l','omg@mivoken.cs','Ferenc','Csagrany','User','Szeged',0);
INSERT into FELHASZNALO values('Kati','flkdsafdsakf','kata@hosszu.nincs','Katinka','Hosszu','User','Budapest',0);
INSERT into FELHASZNALO values('Barna','3f3f333','barna@gerdan.hu','Barna','Gerdan','User','Szeged',0);
INSERT into FELHASZNALO values('Zoli','fsdaf3f3','zoltan@al.x','Zoli','Alx','User','Valahol',0);
INSERT into FELHASZNALO values('Wayne','daslfkasfe','wayne@rooney.com','Wayne','Rooney','User','Manchester',0);
INSERT into FELHASZNALO values('Roni','fasdfasfdsfdsa','ronaldo@cristiano.rinya','Cristi','Roni','User','Torino',0);
INSERT into FELHASZNALO values('Edes','fldsakfksadf','gyori@edes.hu','Gyori','Edes','User','Gyor',0);
INSERT into FELHASZNALO values('Candy','fdsafasdfdsaf','candy@crush.com','Can','Cru','User','LosAngeles',0);
INSERT into FELHASZNALO values('Obi','fasdfsdaf','obama@barack.com','Barack','Obama','User','Washington',0);
INSERT into FELHASZNALO values('Doni','fadsfasdf','donald@trump.com','Donald','Trump','User','Washington',0);
INSERT into FELHASZNALO values('Sanyi','fksadf83','sanya@vki.hu','Sanya','Vagyok','User','Salgotarjan',0);
INSERT into FELHASZNALO values('Marton','3klfj3lkfjka','marton@anevem.com','Marton','Alegjobbnev','User','Mindenhol',0);

INSERT into RENDELES values(19,4,1,'Marci',0);
INSERT into RENDELES values(20,7,1,'Marci',0);
INSERT into RENDELES values(21,23,1,'Marci',0);
INSERT into RENDELES values(22,30,1,'Marci',0);
INSERT into RENDELES values(23,14,1,'Marci',0);
INSERT into RENDELES values(24,16,1,'Marci',0);
INSERT into RENDELES values(25,17,1,'Marci',0);
INSERT into RENDELES values(26,18,1,'Marci',0);
INSERT into RENDELES values(27,11,1,'Marci',0);
INSERT into RENDELES values(28,28,1,'Marci',0);
INSERT into RENDELES values(29,15,1,'Marci',0);
INSERT into RENDELES values(30,27,1,'Marci',0);
INSERT into RENDELES values(31,0,1,'Marci',0);
INSERT into RENDELES values(32,9,1,'Marci',0);
INSERT into RENDELES values(33,8,1,'Marci',0);
INSERT into RENDELES values(34,19,1,'Marci',0);
INSERT into RENDELES values(35,31,1,'Marci',0);
INSERT into RENDELES values(36,13,1,'Marci',0);
INSERT into RENDELES values(37,25,1,'Marci',0);
INSERT into RENDELES values(38,24,1,'Marci',0);

INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(20000,'Csabi','csaba@major.nincs',0,0);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(12000,'Zoli','zoltan@al.x',0,1);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(8800,'Obi','obama@barack.com',0,2);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(13400,'Feci','omg@mivoken.cs',0,3);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(6700,'Kati','kata@hosszu.nincs',0,4);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(30000,'Wayne','wayne@rooney.com',0,5);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(22300,'Doni','donald@trump.com',0,6);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(20000,'Csabi','csaba@major.nincs',0,7);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(12000,'Zoli','zoltan@al.x',0,8);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(8800,'Obi','obama@barack.com',0,9);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(13400,'Feci','omg@mivoken.cs',0,10);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(6700,'Kati','kata@hosszu.nincs',0,11);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(30000,'Wayne','wayne@rooney.com',0,12);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(22300,'Doni','donald@trump.com',0,13);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(12000,'Zoli','zoltan@al.x',0,14);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(8800,'Obi','obama@barack.com',0,15);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(13400,'Feci','omg@mivoken.cs',0,16);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(6700,'Kati','kata@hosszu.nincs',0,17);
INSERT into SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) values(6700,'Kati','kata@hosszu.nincs',0,18);