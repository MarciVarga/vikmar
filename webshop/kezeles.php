<!DOCTYPE HTML>
<html>

<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>

		<title>Vikmar Webshop</title>
		
		#customers {
		  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		  border-collapse: collapse;
		  width: 100%;
		}

		#customers td, #customers th {
		  border: 1px solid #ddd;
		  padding: 12px;
		  font-size: 18px;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers tr:hover {background-color: #ddd;}

		#customers th {
		  padding-top: 12px;
		  padding-bottom: 12px;
		  text-align: left;
		  background-color: #4CAF50;
		  color: white;
		}
		
		.btn {
		  background-color: black;
		  border: none;
		  color: white;
		  padding: 12px 16px;
		  font-size: 20px;
		  cursor: pointer;
		  border-radius: 50%;
		}

		.btn:hover {
		  background-color: white;
		  color: black;
		}

	</style>

</head>


<body>

<?php

	include("db.php");
	include_once 'admin.php';
	
	$trigger = oci_parse($conn, "create or replace trigger tr1 after update on termek for each row begin dbms_output.put_line('Frissitve'); end;");
	oci_execute($trigger);
	
	echo '<b><table id="customers" align="center" style="text-align:center;">';
	
	$conn = dbjoin();
	
		if ( !($conn = dbjoin()) ) {
			  return false;
			}
			
			$stid = oci_parse( $conn,"SELECT termekid, nev, ar, hazhozar as Hazhozszallitas, keszlet, termekid as tid FROM Termek");
			oci_execute($stid);
			
			oci_close($conn);
			
			while( $row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS))
			foreach($row as $item)
		
		
		
		
			$nfields = oci_num_fields($stid);
			echo '<tr >';
			for ($i = 1; $i<=$nfields; $i++){
				if($i == 1) {
					echo '<td>TORLES</td>';
				} else if($i == 6) {
					echo '<td>MODOSITAS</td>';
				} else {
					$field = oci_field_name($stid, $i);
					echo '<td>' . $field . '</td>';
				}
			}
			echo '</tr>';
			

			//// -- ujra vegrehajtom a lekerdezest, es kiiratom a sorokat
			oci_execute($stid);

			$kellft = 0;
			
			while ( $row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) {
				echo '<tr>';
				foreach ($row as $item) {
					if ( $kellft == 2 || $kellft == 3) {
						echo '<td>' . $item . ' Ft</td>';
					} else if ($kellft == 0) {
						echo '<td><form method="POST" action="torles.php">
							<button class="btn" type="submit" name="termekid" value='.$item.'><i class="fa fa-trash-o"></i></button></td></form>';
					} else if ($kellft == 5) {
						echo '<td><form method="POST" action="torles.php"><button class="btn" type="submit" name="termekidle" value='.$item.'><i class="fa fa-sort-desc"></i></button>
								<form method="POST" action="torles.php"><button class="btn" type="submit" name="termekidfel" value='.$item.'><i class="fa fa-sort-asc"></i></button>
								</td></form>';
					}else {
						echo '<td>' . $item . '</td>';
					}
					$kellft = $kellft + 1;
				}
				echo '</tr>';
				$kellft = 0;
			}
			
			echo '</table><b>';

?>

</body>








</html>