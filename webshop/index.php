<!DOCTYPE html>
<html>
<head>

	<style>

		* {
		  box-sizing: border-box;
		}

		body {
		  font-family: Arial, Helvetica, sans-serif;
		}





		.footer {
		  background-color: #f1f1f1;
		  padding: 10px;
		  text-align: center;
		}

		@media (max-width: 600px) {
		  .row {
			-webkit-flex-direction: column;
			flex-direction: column;
		  }
		}
		
		img {
		  border-radius: 8px;
		}
		
		.fs {
			text-align: center;
			text-shadow: -1px -1px 0px rgba(255,255,255,0.3), 1px 1px 0px rgba(0,0,0,0.8);
			color: #747474;
			opacity: 0.4;
			font: 700 800px 'Bitter';
			font-size: 10vw;
		}
		
		@charset "UTF-8";
		@import url(https://fonts.googleapis.com/css?family=Open+Sans:300,400,700);

		body {
		  font-family: 'Open Sans', sans-serif;
		  font-weight: 300;
		  line-height: 1.42em;
		  color:#A7A1AE;
		  background-color:#1F2739;
		}

		h1 {
		  font-size:3em; 
		  font-weight: 300;
		  line-height:1em;
		  text-align: center;
		  color: #4DC3FA;
		}

		h2 {
		  font-size:3em; 
		  font-weight: 300;
		  text-align: center;
		  display: block;
		  line-height:1em;
		  padding-bottom: 0.5em;
		  color: #01a2e6;
		}

		h2 a {
		  font-weight: 700;
		  text-transform: uppercase;
		  color: #FB667A;
		  text-decoration: none;
		}

		.blue { color: #185875; }
		.yellow { color: #FFF842; }

		.container th h1 {
			  font-weight: bold;
			  font-size: 1em;
		  text-align: left;
		  color: #185875;
		}

		.container td {
			  font-weight: normal;
			  font-size: 1em;
		  -webkit-box-shadow: 0 2px 2px -2px #0E1119;
			   -moz-box-shadow: 0 2px 2px -2px #0E1119;
					box-shadow: 0 2px 2px -2px #0E1119;
		}

		.container {
			  text-align: left;
			  overflow: hidden;
			  width: 80%;
			  margin: 0 auto;
		  display: table;
		  padding: 0 0 4em 0;
		}

		.container td, .container th {
			  padding-bottom: 2%;
			  padding-top: 2%;
		  padding-left:2%;  
		}

		/* Background-color of the odd rows */
		.container tr:nth-child(odd) {
			  background-color: #323C50;
		}

		/* Background-color of the even rows */
		.container tr:nth-child(even) {
			  background-color: #2C3446;
		}

		.container th {
			  background-color: #1F2739;
		}

		.container td:first-child { color: #FB667A; }

		.container tr:hover {
		   background-color: #464A52;
		-webkit-box-shadow: 0 6px 6px -6px #0E1119;
			   -moz-box-shadow: 0 6px 6px -6px #0E1119;
					box-shadow: 0 6px 6px -6px #0E1119;
		}

		.container td:hover {
		  background-color: #ececec;
		  color: #403E10;
		  font-weight: bold;
		  
		  box-shadow: #7F7C21 -1px 1px, #7F7C21 -2px 2px, #7F7C21 -3px 3px, #7F7C21 -4px 4px, #7F7C21 -5px 5px, #7F7C21 -6px 6px;
		  transform: translate3d(6px, -6px, 0);
		  
		  transition-delay: 0s;
			  transition-duration: 0.4s;
			  transition-property: all;
		  transition-timing-function: line;
		}

		@media (max-width: 800px) {
		.container td:nth-child(4),
		.container th:nth-child(4) { display: none; }
		}
	
	</style>

</head>


<body>

<?php
	include 'db.php';
	include_once 'guestgui.php';
?>



	<div align="center"><img border="1" alt="Webshop" src="webshop.jpg" width="auto" height="auto"></div>
	<h1 class="fs">Vikmar Webshop</h1>
	
	<h1><span class="blue">Legújabb Termékek</span>


	<?php
	
		$conn = dbjoin();
		
		function ujtermek($stid, $kat) {
			
			echo '<h2>'.$kat.'</h2>';
			
			echo "<table class='container'>
			<thead>
				<tr>
					<th><h1>Név</h1></th>
					<th><h1>Ár</h1></th>
				</tr>
			</thead>
			<tbody>";
			
			oci_execute($stid);

			$kellft = 0;
			$i = 0;
			
			while ( $row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) {
				echo '<tr>';
				foreach ($row as $item) {
					if ( $kellft == 1) {
						echo '<td>' . $item . ' Ft</td>';
					} else {
						echo '<td>' . $item . '</td>';
					}
					$kellft = $kellft + 1;
				}
				echo '</tr>';
				$kellft = 0;
				$i = $i+1;
				if($i == 5) {
					break;
				}
			}
			
			echo '</tbody></table></b>';
		}
	
		$stid = oci_parse( $conn,"SELECT nev, ar FROM Termek WHERE katnev='Photo' ORDER BY termekid DESC");
		oci_execute($stid);
			
		ujtermek($stid, 'Photo');
	
		$stid = oci_parse( $conn,"SELECT nev, ar FROM Termek WHERE katnev='Tv' ORDER BY termekid DESC");
		oci_execute($stid);
			
		ujtermek($stid, 'Tv');
		
		$stid = oci_parse( $conn,"SELECT nev, ar FROM Termek WHERE katnev='Computer' ORDER BY termekid DESC");
		oci_execute($stid);
			
		ujtermek($stid, 'Computer');
		
		$stid = oci_parse( $conn,"SELECT nev, ar FROM Termek WHERE katnev='Garden' ORDER BY termekid DESC");
		oci_execute($stid);
			
		ujtermek($stid, 'Garden');
		
		$stid = oci_parse( $conn,"SELECT nev, ar FROM Termek WHERE katnev='Home' ORDER BY termekid DESC");
		oci_execute($stid);
			
		ujtermek($stid, 'Home');
		
		$stid = oci_parse( $conn,"SELECT nev, ar FROM Termek WHERE katnev='Other' ORDER BY termekid DESC");
		oci_execute($stid);
			
		ujtermek($stid, 'Other');
	
	
	
	?>
	
		

<?php

	

?>



</body>




</html>