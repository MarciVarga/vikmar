<!DOCTYPE html>
<html>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>

	
	<title>Vikmar Webshop</title>

	<style>
	
		.topnav {
		  overflow: hidden;
		  background-color: #333;
		}

		.topnav a {
		  float: left;
		  color: #f2f2f2;
		  text-align: center;
		  padding: 14px 16px;
		  text-decoration: none;
		  font-size: 17px;
		}
		
		.topnav a:hover {
		  background-color: #99b3ff;
		  text-shadow: 2px 2px 2px black;
		  font-weight:bold;
		  color: #e6ecff;
		}
		
		.topnav a.active{
		  float: left;
		  color: black;
		  background-color: #ff5050;
		  text-align: center;
		  padding: 14px 16px;
		  text-decoration: none;
		  font-size: 17px;
		}
		
		.topnav a.active:hover {
			text-shadow: none;
		}
		
		.dropdown {
		  float: left;
		  overflow: hidden;
		}

		.dropdown .dropbtn {
		  font-size: 16px;  
		  border: none;
		  outline: none;
		  color: white;
		  padding: 14px 16px;
		  background-color: inherit;
		  font-family: inherit;
		  margin: 0;
		}
		
		.dropdown-content {
		  display: none;
		  position: absolute;
		  background-color: #f9f9f9;
		  min-width: 160px;
		  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		  z-index: 1;
		}

		.dropdown-content a {
		  float: none;
		  color: black;
		  padding: 12px 16px;
		  text-decoration: none;
		  display: block;
		  text-align: left;
		}
		
		.dropdown-content button {
		  float: none;
		  color: black;
		  padding: 12px 16px;
		  text-decoration: none;
		  display: block;
		  text-align: left;
		  width: 250px;
		}
		
		.topnav button {
		  float: left;
		  color: #f2f2f2;
		  text-align: center;
		  padding: 14px 16px;
		  text-decoration: none;
		  font-size: 17px;
		}
		
		.topnav button:hover {
		  background-color: #99b3ff;
		  text-shadow: 2px 2px 2px black;
		  font-weight:bold;
		  color: #e6ecff;
		}



		.dropdown:hover .dropdown-content {
		  display: block;
		}
		
		body {
		  width: 100%;
		  height: 400px;
		  background-image: url('background.jpg');
		  background-repeat: repeat;
		  background-size: contain;
		}
		
	</style>
</head>
<body>

	<div class="topnav">
	  <a href="indexuser.php">Főoldal</a>
	  <div class="dropdown">
			<button class="dropbtn">Termékek</button>
			<div class="dropdown-content">
				<form method="POST" action="usertermekleker.php">
				<button type="submit" name="termek" value="Photo">Fényképezőgépek</button><br>
				<button type="submit" name="termek" value="Tv">Televíziók</button><br>
				<button type="submit" name="termek" value="Computer">Számítógépek</button><br>
				<button type="submit" name="termek" value="Garden">Kertészeti eszközök</button><br>
				<button type="submit" name="termek" value="Home">Otthon</button><br>
				<button type="submit" name="termek" value="Mobil">Telefonok</button><br>
				<button type="submit" name="termek" value="Dress">Ruhák</button><br>
				<button type="submit" name="termek" value="Other">Egyéb</button>
				</form>
			</div>
		</div>
	  <a href="profil.php">Profil</a>
	  <a href="kosar.php">Kosár</a>
	  <a href="login.php">Kijelentkezés</a>
	  <?php
	  
		include_once 'db.php';
		$conn = dbjoin();
		
		$admine = oci_parse($conn, "select jognev as jn from felhasznalo where azonosito='".$_COOKIE["username"]."'");
		oci_define_by_name($admine, 'JN', $jognev);
		oci_execute($admine);
		oci_fetch($admine);
		
		if($jognev == "Admin") {
			echo '<a class="active" href="admin.php">Admin felület</a>';
		}
	  
	  ?>
	</div>
	
</body>
</html>