<!DOCTYPE html>
<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
	#snackbar {
	  visibility: hidden;
	  min-width: 250px;
	  margin-left: -125px;
	  background-color: #333;
	  color: #fff;
	  text-align: center;
	  border-radius: 2px;
	  padding: 16px;
	  position: fixed;
	  z-index: 1;
	  left: 50%;
	  bottom: 30px;
	  font-size: 17px;
	}

	#snackbar.show {
	  visibility: visible;
	  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
	  animation: fadein 0.5s, fadeout 0.5s 2.5s;
	}

	@-webkit-keyframes fadein {
	  from {bottom: 0; opacity: 0;} 
	  to {bottom: 30px; opacity: 1;}
	}

	@keyframes fadein {
	  from {bottom: 0; opacity: 0;}
	  to {bottom: 30px; opacity: 1;}
	}

	@-webkit-keyframes fadeout {
	  from {bottom: 30px; opacity: 1;} 
	  to {bottom: 0; opacity: 0;}
	}

	@keyframes fadeout {
	  from {bottom: 30px; opacity: 1;}
	  to {bottom: 0; opacity: 0;}
	}
	</style>
</head>

<body>

<?php

	include 'db.php';
	include_once 'usergui.php';
	
	$conn = dbjoin();
	
	$stid = oci_parse($conn, "select sum(termek.ar*rendeles.mennyit)+max(termek.hazhozar) as ossz from termek, rendeles where termek.termekid = rendeles.termekid and rendeles.kifizetve = 0 and rendeles.nev='".$_COOKIE["username"]."'");
	oci_define_by_name($stid, 'OSSZ', $osszeg);
	oci_execute($stid);
	oci_fetch($stid);
	
	$egyenleghez = oci_parse($conn, "select egyenleg as egy, azonosito as az, email as em from felhasznalo where azonosito='".$_COOKIE["username"]."'");
	oci_define_by_name($egyenleghez, 'EGY', $egyenleg);
	oci_define_by_name($egyenleghez, 'AZ', $nev);
	oci_define_by_name($egyenleghez, 'EM', $email);
	oci_execute($egyenleghez);
	oci_fetch($egyenleghez);
	
	if($egyenleg < $osszeg) {
		echo 'Nincs elegendő egyenleged!';
	} else {
		
		$szamla = oci_parse($conn, "select max(szamlaszam) as szszam from szamla");
		oci_define_by_name($szamla, 'SZSZAM', $maxszamla);
		oci_execute($szamla);
		oci_fetch($szamla);
		
		$maxszamla = $maxszamla+1;
		
		$insertes = oci_parse($conn, "INSERT INTO SZAMLA (VEGOSSZEG,USERID,USEREMAIL,KISZALLITVA,SZAMLASZAM) VALUES(".$osszeg.",'".$nev."','".$email."',0,".$maxszamla.")");
		oci_execute($insertes);

		$minusz = oci_parse($conn, "UPDATE felhasznalo SET egyenleg = egyenleg - :fk_os WHERE azonosito = '".$nev."'");
		oci_bind_by_name($minusz, ":fk_os", $osszeg);
		oci_execute($minusz);
		
		$frissit = oci_parse($conn, "UPDATE rendeles SET kifizetve = 1 WHERE nev = '".$nev."'");
		oci_execute($frissit);
		
		echo 'Sikeres vásárlás!';
	}
?>

</body>


</html>