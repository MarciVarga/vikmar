<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

	@import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);

	body {
	  font-family: "Roboto", helvetica, arial, sans-serif;
	  font-size: 16px;
	  font-weight: 400;
	  text-rendering: optimizeLegibility;
	}
	
	thead {
		background-color: black;
		color: white
	}

	div.table-title {
	   display: block;
	  margin: auto;
	  max-width: 600px;
	  padding:5px;
	  width: 100%;
	}

	.table-title h3 {
	   color: #fafafa;
	   font-size: 30px;
	   font-weight: 400;
	   font-style:normal;
	   font-family: "Roboto", helvetica, arial, sans-serif;
	   text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
	   text-transform:uppercase;
	}


	/*** Table Styles **/

	.table-fill {
	  background: white;
	  border-radius:3px;
	  height: 320px;
	  margin: auto;
	  max-width: 600px;
	  padding:5px;
	  width: 100%;
	  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
	  animation: float 5s infinite;
	}
	 
	th {
	  color:#D5DDE5;;
	  background:#1b1e24;
	  border-bottom:4px solid #9ea7af;
	  border-right: 1px solid #343a45;
	  font-size:23px;
	  font-weight: 100;
	  padding:24px;
	  text-align:left;
	  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
	  vertical-align:middle;
	}

	th:first-child {
	  border-top-left-radius:3px;
	}
	 
	th:last-child {
	  border-top-right-radius:3px;
	  border-right:none;
	}
	  
	tr {
	  border-top: 1px solid #C1C3D1;
	  border-bottom-: 1px solid #C1C3D1;
	  color:#666B85;
	  font-size:16px;
	  font-weight:normal;
	  text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
	}
	 
	tr:hover td {
	  background:#4E5066;
	  color:#FFFFFF;
	  border-top: 1px solid #22262e;
	}
	 
	tr:first-child {
	  border-top:none;
	}

	tr:last-child {
	  border-bottom:none;
	}
	 
	tr:nth-child(odd) td {
	  background:#EBEBEB;
	}
	 
	tr:nth-child(odd):hover td {
	  background:#4E5066;
	}

	tr:last-child td:first-child {
	  border-bottom-left-radius:3px;
	}
	 
	tr:last-child td:last-child {
	  border-bottom-right-radius:3px;
	}
	 
	td {
	  background:#FFFFFF;
	  padding:4px;
	  text-align:center;
	  vertical-align:middle;
	  font-weight:300;
	  font-size:18px;
	  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
	  border-right: 1px solid #C1C3D1;
	}

	td:last-child {
	  border-right: 0px;
	}

	.btn {
	  background-color: black;
	  border: none;
	  color: white;
	  padding: 6px 8px;
	  font-size: 12px;
	  cursor: pointer;
	  border-radius: 50%;
	}

	.btn:hover {
	  background-color: white;
	  color: black;
	}
	
	.but {
	  background-color: #4CAF50;
	  border: none;
	  color: white;
	  padding: 16px 32px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 16px;
	  margin: 6px 4px;
	  -webkit-transition-duration: 1s;
	  transition-duration: 0.4s;
	  cursor: pointer;
	}

	.but1 {
	  background-color: white; 
	  color: black; 
	  border: 2px solid #008CBA;
	}
	
	.but2 {
	  background-color: white; 
	  color: black; 
	  border: 2px solid #008CBA;
	}
	
	.but1:hover {
	  background-color: #008CBA;
	  color: white;
	}
	
	.but2:hover {
	  background-color: #008CBA;
	  color: white;
	}

</style>
</head>
<body>

<?php


	include("db.php");
	include_once 'usergui.php';
	
	echo '<table class="table-fill">';
	
	$conn = dbjoin();
	

		if ( !($conn = dbjoin()) ) {
			  return false;
			}
			
			$stid = oci_parse( $conn,"select termek.termekid as torles, termek.nev, rendeles.mennyit as DB, termek.ar, termek.hazhozar from termek, rendeles where rendeles.kifizetve=0 and termek.termekid = rendeles.termekid and rendeles.nev = '".$_COOKIE["username"]."'");
			oci_define_by_name($stid, 'DB', $darab);
			oci_execute($stid);
			oci_fetch($stid);

			echo '<h2 align="center">Kosár</h2>';

				oci_close($conn);
				
				while( $row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS))
				foreach($row as $item)
		
		
				
		
				
				echo '<thead><tr>';
				
				if($darab == NULL) {
					echo '</tr></thead><td>Üres</td></table>';
					return 0;
				}
				
				
				$nfields = oci_num_fields($stid);
				
				for ($i = 1; $i<=$nfields; $i++){
					if($i == 4) {
						echo '<td>AR</td>';
					} else {
						$field = oci_field_name($stid, $i);
						echo '<td>' . $field . '</td>';
					}
				}
				echo '</tr></thead><tbody>';
				

				//// -- ujra vegrehajtom a lekerdezest, es kiiratom a sorokat
				oci_execute($stid);

				$kellft = 0;
				
				while ( $row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) {
					echo '<tr>';
					foreach ($row as $item) {
						if ( $kellft == 2) {
							$db = $item;
						}
						if( $kellft == 3) {
							$item = $item*$db;
						}
						if ( $kellft == 3 || $kellft == 4) {
							echo '<td>' . $item . ' Ft</td>';
						} else if ($kellft == 0) {
							echo '<td><form method="POST" action="kosarbol.php">
								<button class="btn" type="submit" name="termekid" value='.$item.'><i class="fa fa-trash"></i></button></td></form>';
						} else {
							echo '<td>' . $item . '</td>';
						}
						$kellft = $kellft + 1;
					}
					echo '</tr>';
					$kellft = 0;
				}
				
				$stid1 = oci_parse($conn, "select sum(termek.ar*rendeles.mennyit)+max(termek.hazhozar) as ossz from termek, rendeles where termek.termekid = rendeles.termekid and rendeles.kifizetve = 0 and rendeles.nev='".$_COOKIE["username"]."'");
				oci_define_by_name($stid1, 'OSSZ', $osszeg);
				oci_execute($stid1);
				oci_fetch($stid1);
				
				echo '<tr>
					<td>
					<form method="POST" action="usertermekleker.php">
						<button type="submit" class="but but2" value="Photo" name="termek">Vásárlás folytatása</button>
					</form>
					<td><td></td><td></td><td>
					<form method="POST" action="szamlahoz.php">
						<button class="but but1">Fizetés ('.$osszeg.' Ft)</button>
					</form>
					</td></tr>';
				echo '</tbody></table>';
				


			










?>


</body>
</html>