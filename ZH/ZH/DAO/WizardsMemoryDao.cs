﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.Model;

namespace ZH.DAO
{
    class WizardsMemoryDao : WizardsDao
    {
        private static readonly string s_connectionString = @"Data Source=..\..\..\DB\hogwarts.db";

        private IList<Wizard> wizards = new List<Wizard>();

        private List<Wizard> ReadWizardsFromReader(IDataReader reader)
        {
            List<Wizard> wizards = new List<Wizard>();

            while (reader.Read())
            {
                Wizard wizard = new Wizard
                {
                    ID = reader.GetInt32(reader.GetOrdinal("id")),
                    Name = reader.GetString(reader.GetOrdinal("name")),
                    Height = reader.GetInt32(reader.GetOrdinal("height")),
                    PureBlood = reader.GetBoolean(reader.GetOrdinal("pureblood"))
                };

                wizards.Add(wizard);
            }

            return wizards;
        }

        private Wizard FindWizardByName(SQLiteConnection connection, Wizard wizard)
        {
            Wizard rWizard = null;

            using (SQLiteCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Wizards WHERE name = @name";

                command.Parameters.Add("name", DbType.String).Value = wizard.Name;

                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    List<Wizard> wizards = ReadWizardsFromReader(reader);

                    if (wizards.Count > 0)
                    {
                        rWizard = wizards[0];
                    }
                }
            }

            return rWizard;
        }

        public bool AddWizard(Wizard wizard)
        {
            if (wizard == null)
                return false;

            using (SQLiteConnection conn = new SQLiteConnection(s_connectionString))
            {
                conn.Open();

                Wizard storedWizard = FindWizardByName(conn, wizard);

                if (storedWizard != null)
                    return false;

                SQLiteCommand command = conn.CreateCommand();

                command.CommandText = "INSERT INTO Wizards (name, height, pureblood, houseid) VALUES (@name, @height, @pureblood, @houseid)";

                command.Parameters.Add("name", DbType.String).Value = wizard.Name;
                command.Parameters.Add("height", DbType.Int32).Value = wizard.Height;
                command.Parameters.Add("pureblood", DbType.Boolean).Value = wizard.PureBlood;
                command.Parameters.Add("houseid", DbType.Int32).Value = wizard.HouseID;

                int affectedRows = command.ExecuteNonQuery();

                if (affectedRows != 1)
                    return false;
            }

            return true;
        }

        public IEnumerable<Wizard> GetWizards(int houseId)
        {
            List<Wizard> wizards = null;
            using (SQLiteConnection conn = new SQLiteConnection(s_connectionString))
            using (SQLiteCommand command = conn.CreateCommand())
            {
                conn.Open();

                command.CommandText =
                    "SELECT * " +
                    "FROM Wizards WHERE houseid = @houseid";

                command.Parameters.Add("houseid", DbType.Int32).Value = houseId;

                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    wizards = ReadWizardsFromReader(reader);
                }
            }

            return wizards;
        }

        public IEnumerable<Wizard> GetWizards2()
        {
            List<Wizard> wizards = null;
            using (SQLiteConnection conn = new SQLiteConnection(s_connectionString))
            using (SQLiteCommand command = conn.CreateCommand())
            {
                

                command.CommandText =
                    "SELECT * " +
                    "FROM Wizards";

                conn.Open();

                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    wizards = ReadWizardsFromReader(reader);
                }
            }

            return wizards;
        }
    }
}
