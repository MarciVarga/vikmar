﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.Model;

namespace ZH.DAO
{
    interface WizardsDao
    {
        bool AddWizard(Wizard wizard);
        IEnumerable<Wizard> GetWizards(int houseId);
        IEnumerable<Wizard> GetWizards2();
    }
}
