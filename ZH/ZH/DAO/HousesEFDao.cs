﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.Model;

namespace ZH.DAO
{
    class HousesEFDao : HousesDao
    {
        public IEnumerable<House> GetHouses()
        {
            using (var db = new EFContext())
            {
                return db.Houses.ToList();
            }
        }

        public House GetHouse(int houseId)
        {
            using (var db = new EFContext())
            {
                return db.Houses.FirstOrDefault(h => h.ID == houseId);
            }
        }

        public int HousesCount()
        {
            throw new NotImplementedException();
        }
    }
}
