﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.Model;

namespace ZH.DAO
{
    class WizardsEFDao : WizardsDao
    {
        public bool AddWizard(Wizard wizard)
        {
            using (var db = new EFContext())
            {
                db.Wizards.Add(wizard);
                db.SaveChanges();
            }
            return true;
        }

        public IEnumerable<Wizard> GetWizards(int houseId)
        {
            using (var db = new EFContext())
            {
                return db.Wizards.ToList();
            }
        }

        public IEnumerable<Wizard> GetWizards2()
        {
            using (var db = new EFContext())
            {
                return db.Wizards.ToList();
            }
        }
    }
}
