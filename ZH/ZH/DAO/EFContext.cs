﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.Model;

namespace ZH.DAO
{
    class EFContext : DbContext
    {
        public EFContext() : base("EFContext") { }
        public DbSet<Wizard> Wizards { get; set; }
        public DbSet<House> Houses { get; set; }
    }
}
