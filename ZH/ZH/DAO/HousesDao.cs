﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.Model;

namespace ZH.DAO
{
    interface HousesDao
    {
        IEnumerable<House> GetHouses();
        int HousesCount();
        House GetHouse(int houseId);
    }
}
