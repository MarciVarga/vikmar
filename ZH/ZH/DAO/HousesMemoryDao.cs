﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.Model;

namespace ZH.DAO
{
    class HousesMemoryDao : HousesDao
    {
        private static readonly string s_connectionString = @"Data Source=..\..\..\DB\hogwarts.db";

        private IList<House> houses = new List<House>();

        private List<House> ReadHousesFromReader(IDataReader reader)
        {
            List<House> houses = new List<House>();

            while (reader.Read())
            {
                House house = new House
                {
                    ID = reader.GetInt32(reader.GetOrdinal("id")),
                    Name = reader.GetString(reader.GetOrdinal("name")),
                };

                houses.Add(house);
            }

            return houses;
        }

        private House FindHouseByName(SQLiteConnection connection, House house)
        {
            House rHouse = null;

            using (SQLiteCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Houses WHERE name = @name";

                command.Parameters.Add("name", DbType.String).Value = house.Name;

                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    List<House> houses = ReadHousesFromReader(reader);

                    if (houses.Count > 0)
                    {
                        rHouse = houses[0];
                    }
                }
            }

            return rHouse;
        }

        public IEnumerable<House> GetHouses()
        {
            List<House> houses = null;

            using (SQLiteConnection conn = new SQLiteConnection(s_connectionString))
            using (SQLiteCommand command = conn.CreateCommand())
            {
                command.CommandText =
                    "SELECT * " +
                    "FROM Houses";

                conn.Open();

                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    houses = ReadHousesFromReader(reader);
                }
            }

            return houses;
        }

        public int HousesCount()
        {
            return houses.Count();
        }

        public House GetHouse(int houseId)
        {
            throw new NotImplementedException();
        }
    }
}
