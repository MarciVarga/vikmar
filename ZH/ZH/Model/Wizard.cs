﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZH.Model
{
    class Wizard
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Height { get; set; }
        public bool PureBlood { get; set; }
        public int HouseID { get; set; }
    }
}
