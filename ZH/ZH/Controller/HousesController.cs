﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.DAO;
using ZH.Model;

namespace ZH.Controller
{
    class HousesController
    {
        private readonly HousesDao dao;
        public HousesController(HousesDao housesDao)
        {
            dao = housesDao;
        }
        public IEnumerable<House> GetHouses()
        {
            return dao.GetHouses();
        }
    }
}
