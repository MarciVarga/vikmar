﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZH.DAO;
using ZH.Model;

namespace ZH.Controller
{
    class WizardsController
    {
        private readonly WizardsDao dao;

        public WizardsController(WizardsDao wizardsDao)
        {
            dao = wizardsDao;
        }

        public bool AddWizard(Wizard wizard)
        {
            return dao.AddWizard(wizard);
        }

        public IEnumerable<Wizard> GetWizards(int houseId)
        {
            return dao.GetWizards(houseId);
        }

        public IEnumerable<Wizard> GetWizards2()
        {
            return dao.GetWizards2();
        }
    }
}
