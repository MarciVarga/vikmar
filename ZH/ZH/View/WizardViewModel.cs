﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZH.View
{
    class WizardViewModel
    {
        public string Name { get; set; }
        public bool PureBlood { get; set; }
        public int Height { get; set; }
    }
}
