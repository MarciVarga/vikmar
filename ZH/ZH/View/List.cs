﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZH.Controller;
using ZH.DAO;

namespace ZH.View
{
    partial class List : Form
    {
        private readonly WizardsController controller;
        private readonly HousesController housesController;

        public List()
        {
            InitializeComponent();
            controller = new WizardsController(new WizardsMemoryDao());
        }

        public List(WizardsController controller, HousesController housesController)
        {
            this.controller = controller;
            this.housesController = housesController;
            InitializeComponent();

            List<HouseViewModel> houses = new List<HouseViewModel>();

            foreach (var house in housesController.GetHouses())
            {
                comboBox.Items.Add(house);
            }

            comboBox.Items.AddRange(new string[] { "Maga által készített", "Szerzett", "Veleszületett" });

        }

        private void ListButton_Click(object sender, EventArgs e)
        {
            List<WizardViewModel> wizards = new List<WizardViewModel>();

            foreach (var wizard in controller.GetWizards2())
            {
                wizards.Add(new WizardViewModel
                {
                    Name = wizard.Name,
                    Height = wizard.Height,
                    PureBlood = wizard.PureBlood
                });
            }

            dataGridView.DataSource = null;
            dataGridView.DataSource = wizards;
            dataGridView.Visible = true;
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
