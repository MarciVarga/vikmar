﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZH.View;

namespace ZH
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void AddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Add window = new Add())
            {
                window.ShowDialog();
            }
        }

        private void ListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (List window = new List())
            {
                window.ShowDialog();
            }
        }
    }
}
