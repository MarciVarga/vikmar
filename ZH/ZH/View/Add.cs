﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZH.Controller;
using ZH.DAO;
using ZH.Model;

namespace ZH.View
{
    partial class Add : Form
    {
        private WizardsController controller;
        private int wizardId;

        public Add()
        {
            InitializeComponent();
            controller = new WizardsController(new WizardsMemoryDao());
        }

        public Add(WizardsController controller)
        {
            this.controller = controller;
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            string name = nameTextBox.Text;
            bool pureBlood = purebloodCheckBox.Checked;
            int height = (int)heightNumericUpDown.Value;

            Random rnd = new Random();
            int houseId = rnd.Next(0, 4);

            if (name == string.Empty)
            {
                return;
            }

            Wizard wizard = new Wizard
            {
                Name = name,
                Height = height,
                PureBlood = pureBlood,
                HouseID = houseId
                
            };

            controller.AddWizard(wizard);
        }
    }
}
