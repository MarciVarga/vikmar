import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;

  constructor(private router: Router) { }

  ngOnInit() {
  }


  login(form: NgForm) {
    var submittedForm = form.form.value;
    localStorage.setItem("username", submittedForm.username);
    this.router.navigate(["/main"]);
  }

}
