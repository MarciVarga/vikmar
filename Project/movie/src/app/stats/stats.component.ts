import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets, ChartFontOptions, ChartLegendLabelOptions } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { MovieService } from '../movie.service';
import { Observable } from 'rxjs';
import { preserveWhitespacesDefault } from '@angular/compiler';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class StatsComponent implements OnInit {

  public jsonData: Observable<any>;

  constructor(public movieService : MovieService) { }

  

  ngOnInit() {

    this.movieService.getJson().subscribe(data => {
      this.jsonData = data;
      for(let key in this.jsonData) {
        if(this.pieChartLabels.includes(this.jsonData[key].category) == false) {
          this.pieChartLabels.push(this.jsonData[key].category);
          this.barChartLabels.push(this.jsonData[key].category);
          this.barChartLabels2.push(this.jsonData[key].category);
          this.pieChartData.push(0);
        }
      }


      let szamlalo=[0,0,0,0,0,0];
      let maxhossz=[0,0,0,0,0,0];
      let minhossz=[10000,10000,10000,10000,10000,10000];
      let minrating=[11,11,11,11,11,11];
      let maxrating=[0,0,0,0,0,0];
      let seged;
      //atlagokhoz a szam ertekek
      for(let i in this.pieChartLabels) {
        for(let j in this.jsonData) {
          if(this.jsonData[j].category == this.pieChartLabels[i]) {
            this.pieChartData[i] += this.jsonData[j].mins;
            this.polarData[i] += this.jsonData[j].rating;
            if(this.jsonData[j].mins > maxhossz[i]) {
              maxhossz[i] = this.jsonData[j].mins;
              this.barChartLabels[i] = this.jsonData[j].title;
            }
            if(this.jsonData[j].mins < minhossz[i]) {
              minhossz[i] = this.jsonData[j].mins;
              this.barChartLabels2[i] = this.jsonData[j].title;
            }
            if(this.jsonData[j].rating > maxrating[i]) {
              maxrating[i] = this.jsonData[j].rating;
              this.barChartLabels4[i] = this.jsonData[j].title;
            }
            if(this.jsonData[j].rating < minrating[i]) {
              minrating[i] = this.jsonData[j].rating;
              this.barChartLabels5[i] = this.jsonData[j].title;
            }
            szamlalo[i] += 1;
          }
        }
        this.pieChartData[i] /= szamlalo[i];
        this.polarData[i] /= szamlalo[i];

        this.barChartData[0].data[i] = maxhossz[i];
        this.barChartData2[0].data[i] = minhossz[i];

        this.barChartDataR1[0].data[i] = maxrating[i];
        this.barChartDataR2[0].data[i] = minrating[i];

        this.barChartLabels[i] += '\n-\n';
        this.barChartLabels2[i] += '\n-\n';
        seged = this.pieChartLabels[i];
        this.barChartLabels[i] += seged;
        this.barChartLabels2[i] += seged;

        this.barChartLabels4[i] += '\n-\n';
        this.barChartLabels5[i] += '\n-\n';
        seged = this.pieChartLabels[i];
        this.barChartLabels4[i] += seged;
        this.barChartLabels5[i] += seged;

      }
    });

    for(let i=0;i<this.barChartData.length;i++) {
      console.log(this.barChartData[0].data);
    }

  }

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
      labels: {
        fontColor: 'white',
      }
    },
    plugins: {
      datalabels: {
        formatter: (value: any, ctx: { chart: { data: { labels: { [x: string]: any; }; }; }; dataIndex: string | number; }) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };

  //átlag hossz
  public pieChartLabels: Label[] = [];
  public polarData: number[] = [0,0,0,0,0,0];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;

  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(255,170,0,0.3)', 'rgba(250,255,0,0.3)', 'rgba(12,210,30,0.3)', 'rgba(0,20,255,0.3)', 'rgba(178,0,255,0.3)'],
    },
  ];

 

  //átlag értékelés
  public polarAreaChartLabels: Label[] = [];
  
  public polarAreaChartData: SingleDataSet = this.polarData;
  public polarAreaLegend = true;
  public polarAreaChartType: ChartType = 'polarArea';

  public polarChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
      labels: {
        fontColor: 'white',
      }
    },
  }

  //leghosszabb-legrövidebb film adott kategóriában
  public barChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
      labels: {
        fontColor: 'white',
      }
    },
    scales: { xAxes: [{}], yAxes: [{}] },
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [], label: 'Minutes' },
    //{ data: [28, 48, 40, 19, 86, 27, 90], label: 'Shrotest Movie' }
  ];


  public barChartLabels2: Label[] = [];
  public barChartData2: ChartDataSets[] = [
    { data: [], label: 'Minutes'}
  ];

  //legjobb-legrosszabb értékelés adott kategóriában

  public barChartLabels4: Label[] = [];
  public barChartDataR1: ChartDataSets[] = [
    { data: [], label: 'Rating'}
  ];

  public barChartLabels5: Label[] = [];
  public barChartDataR2: ChartDataSets[] = [
    { data: [], label: 'Rating'}
  ];



  public barChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)','rgba(255,0,0,0.3)','rgba(255,0,0,0.3)','rgba(255,0,0,0.3)','rgba(255,0,0,0.3)','rgba(255,0,0,0.3)'],
    },
  ];

  public barChartColors2 = [
    {
      backgroundColor: ['rgba(0,20,255,0.3)','rgba(0,20,255,0.3)','rgba(0,20,255,0.3)','rgba(0,20,255,0.3)','rgba(0,20,255,0.3)','rgba(0,20,255,0.3)'],
    },
  ];


  


  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }




}
