import { Component, OnInit } from '@angular/core';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  desc: string;
  pic: string;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  tiles: Tile[] = [
    {text: 'Movies', cols: 2, rows: 1, color: '#C70039', desc: 'Movie informations', pic:'asd'},
    {text: 'Stats', cols: 1, rows: 2, color: '#FF5733', desc: 'Compare Movie Categories', pic:'asd1'},
    {text: 'Join Us', cols: 2, rows: 1, color: '#FFC300', desc: 'Login Now!', pic:'asd2'}
  ]

  constructor() {
  }

  ngOnInit() {
  }

}
