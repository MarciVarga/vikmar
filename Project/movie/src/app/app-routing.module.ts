import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { MainComponent } from './main/main.component';
import { StatsComponent } from './stats/stats.component';
import { LoginComponent } from './login/login.component';
import { AuthServiceService } from './auth-service.service';

const routes: Routes = [
  { path: 'main', component: MainComponent, canActivate: [AuthServiceService] },
  { path: 'movies', component: MoviesComponent },
  { path: 'stats', component: StatsComponent },
  { path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
