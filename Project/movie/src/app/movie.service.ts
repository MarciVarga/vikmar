import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class MovieService {

  constructor(private http: HttpClient) {
    this.getJson().subscribe(data => {
      console.log(data);
    });
  }


  public getJson():Observable<any> {
    return this.http.get('./assets/movies.json');
  }
}
