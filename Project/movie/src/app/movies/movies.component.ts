import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {


  public jsonData: Observable<any>;
  public actionData: Observable<any>;

  constructor(public movieService : MovieService) { }

  ngOnInit() {
    this.movieService.getJson().subscribe(data => {
      this.jsonData = data;
      console.log(data);
    });
    console.log('Sikeres JSON fájl beolvasás');
  }

}
