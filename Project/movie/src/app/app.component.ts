import { Component, OnInit } from '@angular/core';
import { MovieService } from './movie.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {


  public jsonData: Observable<any>;

  constructor(public movieService : MovieService, router: Router) {
    router.navigate(['/main']);
  }

  ngOnInit() {
    this.movieService.getJson().subscribe(data => {
      this.jsonData = data;
      console.log(data);
    });
    console.log('Sikeres JSON fájl beolvasás');
  }

  public dI: false;

  public displayInfo(result): Observable<any> {
    console.log(result);
    return result;
  }

  

}

